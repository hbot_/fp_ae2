-- BasicCalc.hs
-- John O'Donnell

{- A basic calculator.  To compiler and run, enter these commands:
      ghc --make BasicCalc
     ./BasicCalc
 -}

 {- Advanced extensions:
 (1) - buttons smaller
     - stack displayed one element per line
     - buttons reordered
 (2) - exception handler was used for parsing input -- the calculator would crash when trying to push empty string to the stack
     - no exception handling was done for 1/0, sqrt (-1), sin (1/0) etc. because it is more helpful 
     - in any case, same type of exception handling as for bEnter would have been employed
 (3) - CLI mode works by running with flag --text; type help for a listing of commands

  (*) - multiple numeric types : have a string flag in the state variable, set the flag in the radio click, 
        - look at the flag in benter, catch the operation exceptions in small functions

 -}

 {-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Graphics.UI.Gtk
import Graphics.UI.Gtk.Glade
import Control.Monad
import Control.Concurrent
import Data.IORef
import Data.Maybe
-- my imports
import qualified Control.Exception as Exc
import System.Environment
import System.Exit

{- This data structure holds the state of the system. -}

data CalcState = CalcState
  { displayString :: String  -- what's displayed at the top
  , stack :: [Double]        -- stack of numbers
  , dispEntry :: Entry       -- the gtk Entry for the display
  , stackBuf :: TextBuffer   -- the gtk TextBuffer for the stack
  , store :: Maybe Double
  }

{- A single state reference sr :: SR is created, which always points
to the current state of the system. -}

type SR = IORef CalcState

{- Initial state of the calculator.  The dispEntry and stackBuf fields
need to be updated with the actual gtk values, which are created
dynamically when the program is initialising itself.  The error values
given in the initState enable the program to crash with an informative
message if these fields are not initialised properly before they are
used. -}

initState :: CalcState
initState = CalcState
  { displayString = ""
  , stack = []
  , dispEntry = error "Display entry not set"
  , stackBuf = error "Stack text buffer not set"
  , store = Nothing
  }

{- The main program initialises the widgets and then starts the
GUI. -}

main :: IO ()
main =
  do initGUI
     timeoutAddFull
       (yield >> return True)
       priorityDefaultIdle 50

-- Read in the glade file

     let gladeFilePath = "glade/calculator.glade"
     maybe_xml <- xmlNew gladeFilePath
     let xml = case maybe_xml of
           Just xml_text -> xml_text
           Nothing -> error "cannot open glade xml file"

-- Set up the main window

     mainWindow <- xmlGetWidget xml castToWindow "MainWindow"
     onDestroy mainWindow mainQuit

-- Initialise the state reference

     sr <- newIORef initState

-- Activate Menu: File: Quit

     quitMenuAction <- xmlGetWidget xml castToMenuItem "menuQuit"
     onActivateLeaf quitMenuAction $ do mainQuit

-- Initialise the display entry (the top field for entering numbers)

     displayEntry <- xmlGetWidget xml castToEntry "DisplayEntry"
     s <- readIORef sr
     writeIORef sr (s {dispEntry = displayEntry})

-- Initialise the stack view (the text view at the bottom)

     stackView <- xmlGetWidget xml castToTextView "StackTextView"
     textBufTagTable <- textTagTableNew
     stackTextBuf <- textBufferNew (Just textBufTagTable)
     textViewSetBuffer stackView stackTextBuf
     textBufferSetText stackTextBuf ""
     s <- readIORef sr
     writeIORef sr (s {stackBuf = stackTextBuf})

-- Set up the digit and decimal point buttons

     forM
       [("b0",'0'), ("b1",'1'), ("b2",'2'), ("b3",'3'), ("b4",'4'),
        ("b5",'5'), ("b6",'6'), ("b7",'7'), ("b8",'8'), ("b9",'9'),
        ("bpoint",'.')]
       (\(x,y) -> prepareNumButton sr xml x y)

-- Set up the Enter button

     benter <- xmlGetWidget xml castToButton "benter"
     onClicked benter $ do
      s <- readIORef sr
      Exc.catch (setStack sr ((read (displayString s) :: Double) : stack s)) (\(e :: Exc.ErrorCall) -> putStrLn $ "Invalid input")
      setDisplay sr ""

-- Clear display

     bCE <- xmlGetWidget xml castToButton "bCE"
     onClicked bCE $ do
       setDisplay sr ""

-- Clear stack

     bCLR <- xmlGetWidget xml castToButton "bCLR"
     onClicked bCLR $ do
       setDisplay sr ""
       setStack sr []

-- Change sign of stack head

     bChangeSign <- xmlGetWidget xml castToButton "bChangeSign"
     onClicked bChangeSign $ do
       s <- readIORef sr
       setStack sr (changeSign (stack s))

-- Store 

     bStore <- xmlGetWidget xml castToButton "bSTO"
     onClicked bStore $ do
       setStore sr

-- Fetch

     bFetch <- xmlGetWidget xml castToButton "bFET"
     onClicked bFetch $ do
       fetchStore sr

-- Exchange first two elements in stack

     bExchange <- xmlGetWidget xml castToButton "bEXCH"
     onClicked bExchange $ do
      s <- readIORef sr
      setStack sr (swapElements (stack s))

-- Radio buttons 

     bIntRadio <- xmlGetWidget xml castToRadioButton "integerRadio"
     onToggled bIntRadio $ do
       selected <- toggleButtonGetActive bIntRadio
       if (selected) then putStrLn $ "Int radio selected" else return ()

       {-

     bFloatRadio <- xmlGetWidget xml castToRadioButton "floatRadio" 
     onToggled bFloatRadio $ do
      selected <- toggleButtonGetActive bFloatRadio
      if (selected) -}

-- Set up the operator buttons

     prepareBinopButton sr xml "bAdd" (+)
     prepareBinopButton sr xml "bSub" (-)
     prepareBinopButton sr xml "bMul" (*)
     prepareBinopButton sr xml "bDiv" (/)
     prepareUnopButton sr xml "bReciprocal" (1/)
     prepareUnopButton sr xml "bSqrt" (sqrt)
     prepareUnopButton sr xml "bSin" (sin)
     prepareUnopButton sr xml "bCos" (cos)

-- Menu Help: About -- Inspired from: http://code.haskell.org/gtk2hs/gtk/demo/carsim/CarSim.hs
     menuAbout <- xmlGetWidget xml castToMenuItem "menuAbout"
     on menuAbout menuItemActivate $ do
      aboutDialog <- aboutDialogNew
      aboutDialogSetName aboutDialog "BasicCalc"
      aboutDialogSetVersion aboutDialog "1.0"
      aboutDialogSetAuthors aboutDialog $ ["me\n" ++ "Dr. Wim\n" ++ "JToD\n"]
      aboutDialogSetComments aboutDialog $ "Assessed Exercise 2 for FP4"
      dialogRun aboutDialog
      widgetDestroy aboutDialog

-- Start up the GUI
     
     args <- getArgs
     case args of 
      (x:xs) -> case x of 
                  "--help" -> putStrLn $ "Usage [--help] [--text]"
                  "--text" -> do
                                putStrLn $ "********************"
                                putStrLn $ "Running in text mode"
                                putStrLn $ "********************"
                                cliMode sr
      [] -> do widgetShowAll mainWindow
               mainGUI

     --widgetShowAll mainWindow
     --mainGUI

{- Changes the sign of the top of the stack-}

changeSign :: [Double] -> [Double]
changeSign [] = []
changeSign (x:xs) = ((-1) * x):xs

{- Access to stack head-}

stackHead :: [Double] -> Maybe Double
stackHead [] = Nothing
stackHead (x:xs) = Just x

{- Swap first two elements in stack-}

swapElements :: [Double] -> [Double]
swapElements (x:y:rest) = y:x:rest
swapElements other = other

{- Set store -}

setStore :: SR -> IO ()
setStore sr = do 
  s <- readIORef sr
  v <- return (stackHead (stack s))
  putStrLn ("Stored: " ++ (show v))
  writeIORef sr (s {store = v})

{- Fetch store -}

fetchStore :: SR -> IO ()
fetchStore sr = do
  s <- readIORef sr
  if (store s) == Nothing then putStrLn ("Nothing stored") 
                          else setStack sr ((fromJust(store s)):(stack s))

{- Run in command line mode -}

cliMode :: SR -> IO ()
cliMode sr = do 
  s <- readIORef sr
  line <- getLine
  case line of 
    "help" -> putStrLn $ 
                 "Commands:   [help][print][exit][store][fetch][exchange]\n"
              ++ "Operations: [+][-][*][/][1/][sin][cos][log]\n"
              --++ "Types:      [integer][float][complex][rational]"
    "print" -> putStrLn ("Stack: " ++ show (stack s))
    "exit"  -> exitSuccess
    "+" -> setStack sr (applyBinOp (stack s) (+))
    "-" -> setStack sr (applyBinOp (stack s) (-))
    "*" -> setStack sr (applyBinOp (stack s) (*))
    "/" -> setStack sr (applyBinOp (stack s) (/))
    "1/" -> setStack sr (applyUnOp (stack s) (1/))
    "sin" -> setStack sr (applyUnOp (stack s) (sin))
    "cos" -> setStack sr (applyUnOp (stack s) (cos))
    "log" -> setStack sr (applyUnOp (stack s) (log))
    "store" -> setStore sr
    "fetch" -> fetchStore sr
    "exchange" -> setStack sr (swapElements (stack s))
    _ -> do
          Exc.catch (setStack sr ((read line :: Double) : (stack s))) (\(e :: Exc.ErrorCall) -> putStrLn $ "Invalid input")
  cliMode sr

{- Binary Operation helper for CLI mode-}

applyBinOp :: [Double] -> (Double -> Double -> Double) -> [Double]
applyBinOp (x:y:rest) f = (f x y):rest
applyBinOp other _ = other

{- Unary Operation helper for CLI mode-}

applyUnOp :: [Double] -> (Double -> Double) -> [Double]
applyUnOp (x:rest) f = (f x):rest
applyUnOp other _ = other

{- Set the stack to xs.  The new stack is shown in the text view on
the GUI, and is also printed to the console. -}

setStack :: SR -> [Double] -> IO ()
setStack sr xs =
  do s <- readIORef sr
     let str = show xs
     textBufferSetText (stackBuf s) (unlines (map show xs)) -- add newline character to the stack textview
     putStrLn ("Stack: " ++ str)
     writeIORef sr (s {stack = xs})

{- Set the display to xs.  This is set in the GUI, and also printed on
the console. -}

setDisplay :: SR -> String -> IO ()
setDisplay sr xs =
  do s <- readIORef sr
     entrySetText (dispEntry s) xs
     writeIORef sr (s {displayString = xs})
     putStrLn xs

{- This function takes several parameters needed to describe an
operator with two operands, such as + or *, and it sets up the
button. -}

prepareBinopButton
  :: SR -> GladeXML -> String -> (Double -> Double -> Double) -> IO ()
prepareBinopButton sr xml bname f =
  do button <- xmlGetWidget xml castToButton bname
     onClicked button $ do
       s <- readIORef sr
       case stack s of
         x:y:stack' ->
           do let r = f x y
              setStack sr (r:stack')
              setDisplay sr (show r)
         _ -> return ()
     return ()

{- This function is similar to prepareBinopButton, but it's for
operators that take only one argument. -}

prepareUnopButton
  :: SR -> GladeXML -> String -> (Double -> Double) -> IO ()
prepareUnopButton sr xml bname f =
  do button <- xmlGetWidget xml castToButton bname
     onClicked button $ do
       s <- readIORef sr
       case stack s of
         x:stack' ->
           do let r = f x
              setStack sr (r:stack')
              setDisplay sr (show r)
         _ -> return ()
     return ()

{- This function sets up a button that is used to enter data into the
display, in particular digits and the decimal point. -}

prepareNumButton :: SR -> GladeXML -> String -> Char -> IO ()
prepareNumButton sr xml bname bchar =
  do button <- xmlGetWidget xml castToButton bname
     onClicked button $ do
       s <- readIORef sr
       let newstr = displayString s ++ [bchar]
       setDisplay sr newstr
     return ()
