{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import Graphics.UI.Gtk
import Graphics.UI.Gtk.Glade
import Control.Monad
import Control.Concurrent
import Data.IORef
import Data.Maybe
-- 
import qualified Control.Exception as Exc
import System.Environment
import System.Exit
import Data.Complex
import Data.Ratio
--
import Data.List
import Control.Arrow(second)


{-Generic number data type-}
data Number = NumInt Integer
            | NumDbl Double
            | NumRat (Ratio Integer)
            | NumCpx (Complex Double)
            deriving (Read, Show, Eq)

{- Defining what operations are available on the generic numbers-}

class Operations a where
  -- Binary operators
    (.+.) :: a -> a -> Maybe a
    (.-.) :: a -> a -> Maybe a
    (.*.) :: a -> a -> Maybe a
    (./.) :: a -> a -> Maybe a
    
    -- Unary operators
    neg :: a -> a
    sin :: a -> a
    cos :: a -> a
    sqrt :: a -> a
    recip :: a -> a

instance Operations Number where
  (.+.) (NumInt a) (NumInt b) = Just $ NumInt (a + b)
  (.+.) (NumDbl a) (NumDbl b) = Just $ NumDbl (a + b)
  (.+.) (NumRat a) (NumRat b) = Just $ NumRat (a + b)
  (.+.) (NumCpx (a :+ b)) (NumCpx (x :+ y)) = Just $ NumCpx ((a + x) :+ (b + y))

  (.-.) (NumInt a) (NumInt b) = Just $ NumInt (a - b)
  (.-.) (NumDbl a) (NumDbl b) = Just $ NumDbl (a - b)
  (.-.) (NumRat a) (NumRat b) = Just $ NumRat (a - b)
  (.-.) (NumCpx (a :+ b)) (NumCpx (x :+ y)) = Just $ NumCpx ((a - x) :+ (b - y))

  (.*.) (NumInt a) (NumInt b) = Just $ NumInt (a * b)
  (.*.) (NumDbl a) (NumDbl b) = Just $ NumDbl (a * b)
  (.*.) (NumRat a) (NumRat b) = Just $ NumRat (a * b)
  (.*.) (NumCpx (a :+ b)) (NumCpx (x :+ y)) = Just $ NumCpx ((a * x - b * y) :+ (b * x + a * y))

  (./.) (NumInt a) (NumInt b) = Just $ NumInt (a `div` b)
  (./.) (NumDbl a) (NumDbl b) = Just $ NumDbl (a / b)
  (./.) (NumRat a) (NumRat b) = Just $ NumRat (a / b)
  (./.) (NumCpx (a :+ b)) (NumCpx (c :+ d)) = Just $ NumCpx ( (a * c + b * d) / ( c * c + d * d) :+  ( b * c  - a * d ) / ( c * c + d * d ) )

  neg (NumInt a) = NumInt ((-1) * a)
  neg (NumDbl a) = NumDbl ((-1) * a)
  neg (NumRat a) = NumRat ((-1) * a)
  neg (NumCpx (a :+ b)) = NumCpx (a :+ ((-1) * b)) -- conjugate

  sin (NumInt a) = NumDbl (Prelude.sin (fromInteger a))
  sin (NumDbl a) = NumDbl (Prelude.sin a)
  sin (NumRat a) = NumDbl (Prelude.sin (fromRational a))
  sin (NumCpx (x :+ y)) = NumCpx ((Prelude.sin x) * (cosh y) :+ (Prelude.cos x) * (sinh y))

  cos (NumInt a) = NumDbl (Prelude.cos (fromInteger a))
  cos (NumDbl a) = NumDbl (Prelude.cos a)
  cos (NumRat a) = NumDbl (Prelude.cos (fromRational a))
  cos (NumCpx (x :+ y)) = NumCpx ((Prelude.cos x) * (cosh y) :+ (Prelude.sin x) * (sinh y))

  sqrt (NumInt a) = NumDbl (Prelude.sqrt (fromInteger a))
  sqrt (NumDbl a) = NumDbl (Prelude.sqrt a)
  sqrt (NumRat a) = NumDbl (Prelude.sqrt (fromRational a))
  -- computes only one root out of the pair of roots for a the complex number, other root can be obtained using neg (z)

  sqrt (NumCpx (a :+ b)) = fromJust (NumCpx (realToFrac (Prelude.sqrt((a + Prelude.sqrt (a * a + b * b)) / 2)) :: Complex Double) .+.
               NumCpx (realToFrac ((signum b) * (Prelude.sqrt ((-1) * a + Prelude.sqrt (a * a + b * b)) / 2)) :: Complex Double))

  recip (NumInt a) = NumDbl (1 / (fromInteger a))
  recip (NumDbl a) = NumDbl (1 / a)
  recip (NumRat a) = NumRat (1 / a)
  recip (NumCpx (x :+ y)) = NumCpx ( ( x / (x * x + y * y)) :+ ((-y) / (x * x + y * y)))

{- This data structure holds the state of the system. -}

data CalcState = CalcState
  { displayString :: String  -- what's displayed at the top
  , stack :: [Number]        -- stack of numbers
  , labelStack :: [String]   -- stack of labels for each number in the stack of numbers
  , dispEntry :: Entry       -- the gtk Entry for the display
  , stackBuf :: TextBuffer   -- the gtk TextBuffer for the stack
  , store :: Maybe Number
  , typeFlag :: String
  , labelFlag :: Bool
  }

{- A single state reference sr :: SR is created, which always points
to the current state of the system. -}

type SR = IORef CalcState

{- Initial state of the calculator.  The dispEntry and stackBuf fields
need to be updated with the actual gtk values, which are created
dynamically when the program is initialising itself.  The error values
given in the initState enable the program to crash with an informative
message if these fields are not initialised properly before they are
used. -}

initState :: CalcState
initState = CalcState
  { displayString = ""
  , stack = []
  , labelStack = []
  , dispEntry = error "Display entry not set"
  , stackBuf = error "Stack text buffer not set"
  , store = Nothing
  , typeFlag = "Integer" -- default calculator type
  , labelFlag = False
  }

main :: IO ()
main =
  do initGUI
     timeoutAddFull
       (yield >> return True)
       priorityDefaultIdle 50

-- Read in the glade file

     let gladeFilePath = "glade/calculator.glade"
     maybe_xml <- xmlNew gladeFilePath
     let xml = case maybe_xml of
           Just xml_text -> xml_text
           Nothing -> error "cannot open glade xml file"

-- Set up the main window

     mainWindow <- xmlGetWidget xml castToWindow "MainWindow"
     onDestroy mainWindow mainQuit

-- Initialise the state reference

     sr <- newIORef initState

-- Activate Menu: File: Quit

     quitMenuAction <- xmlGetWidget xml castToMenuItem "menuQuit"
     onActivateLeaf quitMenuAction $ do mainQuit

-- Initialise the display entry (the top field for entering numbers)

     displayEntry <- xmlGetWidget xml castToEntry "DisplayEntry"
     s <- readIORef sr
     writeIORef sr (s {dispEntry = displayEntry})

-- Initialise the stack view (the text view at the bottom)

     stackView <- xmlGetWidget xml castToTextView "StackTextView"
     textBufTagTable <- textTagTableNew
     stackTextBuf <- textBufferNew (Just textBufTagTable)
     textViewSetBuffer stackView stackTextBuf
     textBufferSetText stackTextBuf ""
     s <- readIORef sr
     writeIORef sr (s {stackBuf = stackTextBuf})

-- Set up the digit and decimal point buttons

     forM
       [("b0",'0'), ("b1",'1'), ("b2",'2'), ("b3",'3'), ("b4",'4'),
        ("b5",'5'), ("b6",'6'), ("b7",'7'), ("b8",'8'), ("b9",'9'),
        ("bpoint",'.')]
       (\(x,y) -> prepareNumButton sr xml x y)

-- Set up the operator buttons

     prepareBinopButton sr xml "bAdd" (.+.)
     prepareBinopButton sr xml "bSub" (.-.)
     prepareBinopButton sr xml "bMul" (.*.)
     prepareBinopButton sr xml "bDiv" (./.)
     prepareUnopButton sr xml "bReciprocal" (Main.recip)
     prepareUnopButton sr xml "bSqrt" (Main.sqrt)
     prepareUnopButton sr xml "bSin" (Main.sin)
     prepareUnopButton sr xml "bCos" (Main.cos)

-- Clear display

     bCE <- xmlGetWidget xml castToButton "bCE"
     onClicked bCE $ do
       setDisplay sr ""

-- Clear stack

     bCLR <- xmlGetWidget xml castToButton "bCLR"
     onClicked bCLR $ do
       setDisplay sr ""
       setStack sr []

-- Store 
     bStore <- xmlGetWidget xml castToButton "bSTO"
     onClicked bStore $ do
       setStore sr

-- Fetch
     bFetch <- xmlGetWidget xml castToButton "bFET"
     onClicked bFetch $ do
       fetchStore sr

-- Exchange first two elements in stack
     bExchange <- xmlGetWidget xml castToButton "bEXCH"
     onClicked bExchange $ do
      s <- readIORef sr
      setStack sr (swapElements (stack s))

-- Set up the Enter button

     benter <- xmlGetWidget xml castToButton "benter"
     onClicked benter $ do
      s <- readIORef sr
      m <- entryGetText (dispEntry s)
      case (labelFlag s) of
        False -> readNumber m sr
        True -> return ()

      readNumber m sr
      setDisplay sr ""

     {- Integer radio button -}

     bIntRadio <- xmlGetWidget xml castToRadioButton "integerRadio"
     onToggled bIntRadio $ do
       selected <- toggleButtonGetActive bIntRadio
       s <- readIORef sr
       if (selected) then writeIORef sr (s {typeFlag = "Integer"}) else return ()

     {- Float radio button -}

     bFloatRadio <- xmlGetWidget xml castToRadioButton "floatRadio"
     onToggled bFloatRadio $ do
       selected <- toggleButtonGetActive bFloatRadio
       s <- readIORef sr
       if (selected) then writeIORef sr (s {typeFlag = "Double"}) else return ()

     {- Complex radio button -}

     bComplexRadio <- xmlGetWidget xml castToRadioButton "complexRadio"
     onToggled bComplexRadio $ do
       selected <- toggleButtonGetActive bComplexRadio
       s <- readIORef sr
       if (selected) then writeIORef sr (s {typeFlag = "Complex"}) else return ()

     {- Rational radio button -}

     bRationalRadio <- xmlGetWidget xml castToRadioButton "rationalRadio"
     onToggled bRationalRadio $ do
       selected <- toggleButtonGetActive bRationalRadio
       s <- readIORef sr
       if (selected) then writeIORef sr (s {typeFlag = "Rational"}) else return ()

-- Menu Help: About -- Inspired from: http://code.haskell.org/gtk2hs/gtk/demo/carsim/CarSim.hs

     menuAbout <- xmlGetWidget xml castToMenuItem "menuAbout"
     on menuAbout menuItemActivate $ do
      aboutDialog <- aboutDialogNew
      aboutDialogSetName aboutDialog "BasicCalc"
      aboutDialogSetVersion aboutDialog "1.0"
      aboutDialogSetAuthors aboutDialog $ ["Horatiu Bota\n" ++ "Dr. Wim Vanderbauwhede\n" ++ "John O'Donnell\n"]
      aboutDialogSetComments aboutDialog $ "Assessed Exercise 2 for FP4"
      dialogRun aboutDialog
      widgetDestroy aboutDialog

-- Toggle use of labels 

     menuView <- xmlGetWidget xml castToMenuItem "menuViewLabels"
     on menuView menuItemActivate $ do
       s <- readIORef sr
       writeIORef sr (s {labelFlag = not (labelFlag s), stack = []})
       

-- Start the calculator : text or graphical

     args <- getArgs
     case args of 
      (x:xs) -> case x of 
                  "--help" -> putStrLn $ "Usage [--help] [--text]"
                  "--text" -> do
                                putStrLn $ "********************"
                                putStrLn $ "Running in text mode"
                                putStrLn $ "********************"
                                cliMode sr
      [] -> do widgetShowAll mainWindow
               mainGUI

     --widgetShowAll mainWindow
     --mainGUI

setStack :: SR -> [Number] -> IO ()
setStack sr xs =
  do s <- readIORef sr
     let str = show xs
     textBufferSetText (stackBuf s) (unlines (map show xs)) -- add newline character to the stack textview
     putStrLn ("Stack: " ++ str)
     writeIORef sr (s {stack = xs})

{- Adds a label to the stack of labels -}

{- Set the display to xs.  This is set in the GUI, and also printed on
the console. -}

setDisplay :: SR -> String -> IO ()
setDisplay sr xs =
  do s <- readIORef sr
     entrySetText (dispEntry s) xs
     writeIORef sr (s {displayString = xs})
     putStrLn xs

{- This function takes several parameters needed to describe an
operator with two operands, such as + or *, and it sets up the
button. -}

prepareBinopButton
  :: SR -> GladeXML -> String -> (Number -> Number -> Maybe Number) -> IO ()
prepareBinopButton sr xml bname f =
  do button <- xmlGetWidget xml castToButton bname
     onClicked button $ do
       s <- readIORef sr
       case stack s of
         x:y:stack' ->
           do r <- Exc.evaluate (f x y)
                  `Exc.catch` \(e :: Exc.PatternMatchFail) -> return (Nothing)
              case r of
                Nothing -> putStrLn $ "Cannot perform operation on different numeric types"
                Just n  -> do
                  setStack sr (n:stack')
                  setDisplay sr ""
         _ -> return ()
     return ()

{- This function is similar to prepareBinopButton, but it's for
operators that take only one argument. -}

prepareUnopButton
  :: SR -> GladeXML -> String -> (Number -> Number) -> IO ()
prepareUnopButton sr xml bname f =
  do button <- xmlGetWidget xml castToButton bname
     onClicked button $ do
       s <- readIORef sr
       case stack s of
         x:stack' ->
           do let r = f x
              setStack sr (r:stack')
              setDisplay sr ""
         _ -> return ()
     return ()

{- This function sets up a button that is used to enter data into the
display, in particular digits and the decimal point. -}

prepareNumButton :: SR -> GladeXML -> String -> Char -> IO ()
prepareNumButton sr xml bname bchar =
  do button <- xmlGetWidget xml castToButton bname
     onClicked button $ do
       s <- readIORef sr
       let newstr = displayString s ++ [bchar]
       setDisplay sr newstr
     return ()

{- Access to stack head-}

stackHead :: [Number] -> Maybe Number
stackHead [] = Nothing
stackHead (x:xs) = Just x

{- Swap first two elements in stack-}

swapElements :: [Number] -> [Number]
swapElements (x:y:rest) = y:x:rest
swapElements other = other

{- Set store -}

setStore :: SR -> IO ()
setStore sr = do 
  s <- readIORef sr
  v <- return (stackHead (stack s))
  putStrLn ("Stored: " ++ (show v))
  writeIORef sr (s {store = v})

{- Fetch store -}

fetchStore :: SR -> IO ()
fetchStore sr = do
  s <- readIORef sr
  if (store s) == Nothing then putStrLn ("Nothing stored") 
                          else setStack sr ((fromJust(store s)):(stack s))

readNumber :: String -> SR -> IO ()
readNumber str sr = do
  s <- readIORef sr
  case (typeFlag s) of
          "Integer" -> Exc.catch (setStack sr ( (NumInt (read str :: Integer)) : stack s)) readMismatchHandler 
          "Double" -> Exc.catch (setStack sr ( (NumDbl (read str :: Double)) : stack s)) readMismatchHandler
          "Complex" -> Exc.catch (setStack sr ( (NumCpx (read str :: Complex Double)) : stack s)) readMismatchHandler
          "Rational" -> Exc.catch (setStack sr ( (NumRat (read str :: Ratio Integer)) : stack s)) readMismatchHandler
  return ()

changeType :: String -> SR -> IO ()
changeType str sr = do
  s <- readIORef sr
  writeIORef sr (s {typeFlag = str})
  putStrLn $ "Current type: " ++ str


cliMode :: SR -> IO ()
cliMode sr = do 
  s <- readIORef sr
  line <- getLine
  case line of 
    -- CLI helper commands
    "help" -> putStrLn $ 
                 "Commands:   [help][print][exit][store][fetch][exchange]\n"
              ++ "Operations: [+][-][*][/][1/][sin][cos][log]\n"
              ++ "Types:      [integer][float][complex][rational]\n"
    "print" -> putStrLn ("Stack: " ++ show (stack s))
    "exit"  -> exitSuccess
    
    -- Change numeric type
    "integer" -> changeType "Integer" sr
    "float" -> changeType "Double" sr
    "complex" -> changeType "Complex" sr
    "rational" -> changeType "Rational" sr

    -- Operations 
    "+" -> Exc.catch (setStack sr (applyBinOp (stack s) (.+.))) typeMismatchHandler
    "-" -> Exc.catch (setStack sr (applyBinOp (stack s) (.-.))) typeMismatchHandler
    "*" -> Exc.catch (setStack sr (applyBinOp (stack s) (.*.))) typeMismatchHandler
    "/" -> Exc.catch (setStack sr (applyBinOp (stack s) (./.))) typeMismatchHandler
    "1/" -> setStack sr (applyUnOp (stack s) (Main.recip))
    "sin" -> setStack sr (applyUnOp (stack s) (Main.sin))
    "cos" -> setStack sr (applyUnOp (stack s) (Main.cos))
    --"log" -> setStack sr (applyUnOp (stack s) (Main.log))
    "store" -> setStore sr
    "fetch" -> fetchStore sr
    "exchange" -> setStack sr (swapElements (stack s))
    _ -> do
          readNumber line sr
  cliMode sr

{- Binary Operation helper for CLI mode-}

applyBinOp :: [Number] -> (Number -> Number -> Maybe Number) -> [Number]
applyBinOp (x:y:rest) f = (fromJust (f x y)):rest
applyBinOp other _ = other

{- Unary Operation helper for CLI mode-}

applyUnOp :: [Number] -> (Number -> Number) -> [Number]
applyUnOp (x:rest) f = (f x):rest
applyUnOp other _ = other

{- Helpers for exception handling -}
{- Type mismatch handler for CLI mode-}
typeMismatchHandler :: Exc.PatternMatchFail -> IO ()
typeMismatchHandler _ = do putStrLn $ "Cannot perform operation on different numeric types"

{- Read mismatch handler -}

readMismatchHandler :: Exc.ErrorCall -> IO ()
readMismatchHandler _ = do putStrLn $ "Invalid input"

-- inspired from http://stackoverflow.com/questions/4503958/what-is-the-best-way-to-split-string-by-delimiter-funcionally
break' d = second (drop 1) . break d

split :: String -> Maybe (String,String)
split [] = Nothing
split xs = Just . break' (==':') $ xs
-- end of inspiration

{-
tuple :: Maybe (String, String) -> SR -> Maybe (String, Number)
tuple (Just (label, num)) sr = Just (label, (readNumber num sr))
tuple _ sr = Nothing

parseString :: String -> Maybe (String, Number)
parseString s = tuple (split s)
--parseString _ = Nothing
-}
